# Rekrutacja Columbus Polska - ZADANIE TECHNICZNE

### Przygotowanie do zadania

Przed przystąpieniem do zadania wykonaj fork tego repozytorium, na którym dostarczysz nam rozwiązanie.
[How to fork on Bitbucket](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)

### Treść zadania

Twoim zadaniem jest zaimplementowanie poniższych funkcjonalności w oparciu o solucje z repozytorium.

- Jako gość powiniennem widzieć liste wszystkich pracowników. Lista musi byc stronnicowana.
- Jako gość powiniennem mieć możliwość wyświetlenia szczgółów każdego pracownika
- Jako gość powiniennem mieć możliwość przefiltrowania listy pracowników po ich stanowisku. Lista powinna byc stronnicowana.

### Solucja
- Dostarczona solucja zawiera projekt aplikacji MVC opartej o .NET Framework
- Do solucji mozesz dodawac nowe projekty

### Dane
- Źródłem danych do zadania jest serwis `REST API`, do którego adres otrzymałeś w mailu
- Autoryzacja do serwisu odbywa się za pomocą headera `ApiKey`, ktorego wartość również otrzymałeś w mailu
- Serwis posiada dokumentacje w postaci `Swagger UI`

### Wymagania techniczne
- Rozwiązanie powinno być zaimplementowane za pomoca `ASP.NET MVC`, w taki sposób, że każda akcja użytkownika wymaga przeładowania strony (nie chcemy sprawdzać umiejętności związanych z javascript)
- Solucja musi sie kompilować
- Funkcjonalności aplikacji muszą działać  
- Kod rozwiązania powinien być `najwyższej jakości produkcyjnej`.  (Podczas oceny zadania bedziemy sie skupiać głównie na tym aspekcie.)
- Aplikacja powinna być przetestowana jednostkowo

### Aspekty wizualne
- Aspekty wizualne aplikacji będą pomijane podczas oceny zadania

### Dostarczenie rozwiazania
- Jako rozwiazania zadania powinienneś dostarczyć nam link do repozytorium, na którym znajduje się `fork` z rozwiazaniem zadania

### Pomoc i Pytania
- Jesli będziesz miał jakiekolwiek pytania dotyczące zadania, skontaktuj odpowiedz na ostatniego maila. 


Powodzenia