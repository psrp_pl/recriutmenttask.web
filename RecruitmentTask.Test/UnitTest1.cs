using NUnit.Framework;
using RecruitmentTask.Web;

namespace RecruitmentTask.Test
{
    [TestFixture]
    public class Tests
    {
        string url = "https://columbusrecruitmenttaskservice.azurewebsites.net/api/Employees";
        string token = "6b5f9789-09c4-46ac-8af5-afa0cba47fdb";
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void TestGetEmployees()
        {
            RecruitmentTask.Web.Services.GetDataRepository getData = new Web.Services.GetDataRepository();
            
            Assert.IsNotNull(getData.GetEmployees(url, token, 6,3));
        }

        [Test]
        public void TestGetEmployeeDetails()
        {
            RecruitmentTask.Web.Services.GetDataRepository getData = new Web.Services.GetDataRepository();
            Assert.IsNotNull(getData.GetEmployeeDetails(3,url,token));
        }
    }
}