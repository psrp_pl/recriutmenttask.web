﻿namespace RecruitmentTask.Web.Models
{
    public class Links
    {
        public string href { get; set; }
        public string rel { get; set; }
        public string method { get; set; }
    }
}