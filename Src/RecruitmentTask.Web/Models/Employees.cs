﻿namespace RecruitmentTask.Web.Models
{
    public class Employees
    {
        public int id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string jobTitle { get; set; }
        public Links[] links { get; set; }
    }
}