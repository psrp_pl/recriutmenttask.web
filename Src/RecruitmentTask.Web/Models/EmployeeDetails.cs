﻿using System;

namespace RecruitmentTask.Web.Models
{
    public class EmployeeDetails
    {
        public DateTime dayOfEmployment { get; set; }
        public string phoneNumber { get; set; }
        public string jobTitle { get; set; }
        public string salary { get; set; }
        public string email { get; set; }

        public Address address { get; set; }
    }
}