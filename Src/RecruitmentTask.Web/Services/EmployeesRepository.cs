﻿using RecruitmentTask.Web.Models;
using System;
using System.Collections.Generic;

namespace RecruitmentTask.Web.Services
{
    public class EmployeesRepository : IEmployeesRepository
    {
        private readonly IGetDataRepository getData = new GetDataRepository();

        public List<Employees> Get(string apiUrl, string token, int skip = 0, int take = 20)
        {
            take = Convert.ToInt32(System.Web.Configuration.WebConfigurationManager.AppSettings["noOfElements"]);
            List<Employees> employees = getData.GetEmployees(apiUrl, token, skip, take);

            return employees;
            
        }

    }
}