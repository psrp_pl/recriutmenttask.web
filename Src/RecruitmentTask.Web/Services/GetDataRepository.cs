﻿using RecruitmentTask.Web.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using System;

namespace RecruitmentTask.Web.Services
{
    public class GetDataRepository : WebClient, IGetDataRepository
    {
        public HttpWebRequest CreateRequest(string url, string token)
        {
            HttpWebRequest request;
            try
            {
                request = HttpWebRequest.CreateHttp(url);
                request.Headers.Add("ApiKey", token);
                request.Accept = "application/json";
            }
            catch (Exception ex)
            {
                return null;
            }
            return request;
        }

        public EmployeeDetails GetEmployeeDetails(int id, string apiUrl, string token)
        {
            string url = $"{apiUrl}/"+id;
            try
            {
                var json = JsonConvert.DeserializeObject<EmployeeDetails>(GetResponse(CreateRequest(url,token)));

                return json;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Employees> GetEmployees(string apiUrl, string token, int skip, int take)
        {
            string url = $"{apiUrl}?skip={skip}&take={take}";
            try
            {
                var json = JsonConvert.DeserializeObject<List<Employees>>(GetResponse(CreateRequest(url,token))).ToList();

                return json;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public string GetResponse(HttpWebRequest request)
        {
            if (request != null)
            {
                using (HttpWebResponse response = GetWebResponse(request) as HttpWebResponse)
                {
                    return new StreamReader(response.GetResponseStream()).ReadToEnd();
                }
            }
            else
            {
                return "";
            }

        }
    }
}