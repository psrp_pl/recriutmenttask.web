﻿using RecruitmentTask.Web.Models;

namespace RecruitmentTask.Web.Services
{
    public class DetailsRepository : IDetailsRepository
    {
        private readonly IGetDataRepository getData = new GetDataRepository();
        public EmployeeDetails GetDetails(int id, string apiUrl,string token)
        {
            EmployeeDetails details = getData.GetEmployeeDetails(id,apiUrl,token);

            return details;
        }
    }
}