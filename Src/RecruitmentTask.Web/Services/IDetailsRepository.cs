﻿using RecruitmentTask.Web.Models;

namespace RecruitmentTask.Web.Services
{
    public interface IDetailsRepository
    {
        EmployeeDetails GetDetails(int id, string apiUrl, string token);
    }
}