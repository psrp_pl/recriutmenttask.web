﻿using System.Collections.Generic;
using RecruitmentTask.Web.Models;

namespace RecruitmentTask.Web.Services
{
    public interface IEmployeesRepository
    {
        List<Employees> Get(string apiUrl, string token, int skip = 0, int take = 20);
    }
}