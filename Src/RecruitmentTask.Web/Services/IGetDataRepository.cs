﻿using RecruitmentTask.Web.Models;
using System.Collections.Generic;
using System.Net;

namespace RecruitmentTask.Web.Services
{
    public interface IGetDataRepository
    {
        HttpWebRequest CreateRequest(string url, string token);
        string GetResponse(HttpWebRequest request);
        List<Employees> GetEmployees(string apiUrl, string token, int skip, int take);
        EmployeeDetails GetEmployeeDetails(int id, string apiUrl, string token);
    }
}