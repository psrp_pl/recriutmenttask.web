﻿using RecruitmentTask.Web.Services;
using System.Collections.Generic;
using System.Web.Mvc;
using RecruitmentTask.Web.Models;
using System.Linq;

namespace RecruitmentTask.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IEmployeesRepository employees = new EmployeesRepository();
        private readonly IDetailsRepository details = new DetailsRepository();
        List<SelectListItem> listItems;
        private string apiURL = System.Web.Configuration.WebConfigurationManager.AppSettings["apiURL"];
        private string apiKey = System.Web.Configuration.WebConfigurationManager.AppSettings["ApiKey"];

        public ActionResult Index(string job = null, int skip =0)
        {
            List<Employees> model;
            
            List<Employees> employeesList = employees.Get(apiURL,apiKey, skip);
            if (employeesList != null)
            {
                List<string> jobs = employeesList.Select(x => x.jobTitle).Distinct().ToList();

                listItems = new List<SelectListItem>();
                listItems.Add(new SelectListItem { Text = "All", Value = "All" });
                foreach (var item in jobs)
                {
                    listItems.Add(new SelectListItem { Text = item, Value = item });
                }

                ViewBag.ListItems = listItems;
                ViewBag.StartIndex = employeesList.First().id - 1;

                if (string.IsNullOrEmpty(job) || job == "All")
                {
                    model = employeesList;
                    ViewBag.JobFilter = "All";
                }
                else
                {
                    model = employeesList.Where(x => x.jobTitle == job).ToList();
                    ViewBag.JobFilter = job;
                }

                return View(model);
            }
            else
            {
                return HttpNotFound();
            }
        }

        public ActionResult Details(int id)
        {
            EmployeeDetails employeeDetails = details.GetDetails(id, apiURL, apiKey);
            if (employeeDetails!=null)
            {
                return View(employeeDetails);
            }
            else
            {
                return HttpNotFound();
            }
            
        }


    }
}